import { StackNavigator } from 'react-navigation';

import HomeScreen from './containers/Splash';
import MainScreen from './containers/Main';
import PostScreen from './containers/Post';
import AddScreen from './containers/AddPost';

const MainNavigation = new StackNavigator({
  Home: { screen: HomeScreen },
  Main: { screen: MainScreen },
  ShowPost: { screen: PostScreen },
  AddPost: { screen: AddScreen }
}, {
  headerMode: 'screen'
});

export default MainNavigation;