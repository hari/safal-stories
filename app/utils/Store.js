import { AsyncStorage } from 'react-native'
export default class Store {

  static KEY_CATEGORIES = 'cats';

  static async getValue(key, defValue = null) {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        return value;
      }
    } catch (err) {
      //
    }
    return defValue;
  }

  static put(key, value) {
    AsyncStorage.setItem(key, value, (err) => {
      if (err) console.log(err);
    });
  }

  static async remove(key) {
    try {
      return await AsyncStorage.removeItem(key);
    } catch (e) {

    }
    return null;
  }
}