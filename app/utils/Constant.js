export default {
  KEY_CATEGORY: 'categories',
  KEY_FEED_TYPE: 'feed_type',
  KEY_NOT_FIRST_RUN: 'not',
  KEY_POST: 'Post',
  KEY_CATEGORY: 'Category',
  NAV_MAIN: 'Main',
  NAV_HOME: 'Home',
  NAV_SHOW_POST: 'ShowPost',
  /** Categories to exlcude for users to choose as they will be in menu */
  EXCLUDE_CATEGORY: [18, 15]
}