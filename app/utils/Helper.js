import { NetInfo } from 'react-native';
import Store from './Store';

export default class Helper {

  static KEY_FIRST_RUN = '_first_run';
  
  static isFirstRun() {
    return Store.getValue(Helper.KEY_FIRST_RUN, true);
  }

  static setFirstRun(value) {
    Store.put(Helper.KEY_FIRST_RUN, value);
  }

}