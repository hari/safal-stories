/**
 * An in-memory store for storing temporary items while app is running
 */
export default class Session {

  static store = [];
  static callbacks = [];

  static put(key, value) {
    if (Session.has(key)) {
      // we will update the item
      Session.store.map(item => {
        if (item.key == key) {
          item.value = value;
        }
        return item;
      });
    } else {
      // add new item
      Session.store.push({ key, value});
    }
    for(i in Session.callbacks) {
      // call the callbacks that have subscribed for change
      Session.callbacks[i]({ key, value });
    }
  }

  static has(key) {
    return Session.store.filter(item => item.key == key).length > 0;
  }

  static get(key, defValue) {
    if (Session.has(key)) {
      return Session.store.filter(item => item.key == key)[0];
    }
    return { key: key, value: defValue };
  }

  static subscribe(callback) {
    return Session.callbacks.push(callback);
  }

  static unsubscribe(index) {
    Session.callbacks.splice(index, 1);
  }

  static all() {
    return Session.store;
  }

}