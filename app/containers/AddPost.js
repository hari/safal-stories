import React, { Component } from 'react';
import { ScrollView, Image, Text, TextInput, View, Alert, StyleSheet, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Icon, FormLabel, Button } from 'react-native-elements';
import API from '../api/ApiRequest';

export default class FragSend extends Component {

  static navigationOptions = {
    title: 'Submit story',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      imageText: 'Add image',
      image: null,
      error: {
        story: '',
        name: ''
      },
      input: {
        story: '',
        name: ''
      },
      busy: false
    };
  }

  async onSendPressed() {
    if (this.state.busy === true) return;
    if (this.state.input.name.trim().length == 0) {
      return this.setState({
        error: {
          name: 'Opps, you forgot to fill the `title` field.'
        }
      });
    }
    if (!this.state.input.story || this.state.input.story.trim().length == 0) {
      return this.setState({
        error: {
          story: 'Opps, you forgot to fill the `story` field.'
        }
      });
    }
    this.setState({  busy: true });
    const message = await API.newStory(this.state.image, this.state.input.name, this.state.input.story);
    if (message && message.status && message.status == 'success') {
      Alert.alert('Success', 'We have received your story.');
    } else {
      Alert.alert('Oops!', 'We encountered an error during your submission.');
    }
    this.setState({  busy: false });
  }

  onInput(type, value) {
    if (type == 'story') {
      const lastName = this.state.input.name;
      this.setState({
        input: {
          story: value,
          name: lastName
        },
        error: {
          email: ''
        },
        busy: false
      });
    } else {
      const lastStory = this.state.input.story;
      this.setState({
        input: {
          name: value,
          story: lastStory
        },
        error: {
          name: ''
        },
        busy: false
      });
    }
  }

  goBack() {
    this.props.navigation.goBack();
  }

  getLoading() {
    return <ActivityIndicator size='small' animating color='#008A7C' />;
  }

  chooseImage() {
    // try {
    //   Picker.showImagePicker({
    //     title: 'Choose image',
    //     mediaType: 'image'
    //   }, (response => {
    //     if (response.didCancel || response.error) {
    //       return;
    //     }
    //     const exts = response.fileName.split('.');
    //     let ext = exts[exts.length - 1];
    //     if (ext === 'jpg') ext = 'jpeg';
    //     const type = 'image/'+ext.toLowerCase();
    //     this.setState({ image: { type, uri: response.uri }, imageText: 'Change image' });
    //   }).bind(this));
    // } catch (e) {
    //   Alert.alert('Oops!', 'Error occured.');
    // }
  }

  render() {
    const control = this.state.busy ? this.getLoading() : null;
    const imageViewOrNull = this.state.image === null ? null : <Image style={{ minHeight: 150 }} source={{ uri: this.state.image.uri }} />
    return (
      <ScrollView style={{ backgroundColor: '#fff' }}>
        <View elevation={2} style={styles.header}>
          <TouchableOpacity style={{ flex: .1, paddingTop: 2 }} onPress={this.goBack.bind(this)}>
            <Icon name='arrow-left' type='simple-line-icon' color='#fff' />
          </TouchableOpacity>
          <Text style={{ paddingLeft: 24, flex: .7, textAlign: 'left', color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Share your story</Text>
          <TouchableOpacity style={{ flex: .2, paddingTop: 4, alignItems: 'flex-end' }} onPress={this.onSendPressed.bind(this)}>
            <Text style={{ color: '#fff', paddingRight: 4, fontSize: 12 }}>PUBLISH</Text>
          </TouchableOpacity>
        </View>
        <View style={{ padding: 16 }}>
          <TextInput placeholderTextColor='#999' multiline={true} numberOfLines={2} editable placeholder='Title...'  onChangeText={ this.onInput.bind(this, 'name') } />
          <Text style={styles.error}>{this.state.error.name}</Text>
          <TextInput numberOfLines={8} placeholderTextColor='#999' placeholder='Your story...' multiline editable onChangeText={ this.onInput.bind(this, 'story') } />
          <Text style={styles.error}>{this.state.error.story}</Text>
          <TouchableOpacity onPress={this.chooseImage.bind(this)}>
            <Text style={{ marginTop: 8, marginBottom: 8, fontWeight: 'bold' }}>{this.state.imageText}</Text>
            { imageViewOrNull }
          </TouchableOpacity>
          { control }
        </View>
      </ScrollView>
    );
  } 
}

const styles = StyleSheet.create({
  header: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 12,
    paddingTop: 12,
    flex: 1,
    flexDirection: 'row',
    maxHeight: 50,
    backgroundColor: '#008A7C',
    borderBottomWidth: 1,
    borderBottomColor: '#007B6B',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  error: {
    color: '#a44',
    fontSize: 12,
    marginTop: 8,
    marginBottom: 4
  }
})