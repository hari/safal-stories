import React, { Component } from 'react';
import { View, Text, NetInfo } from 'react-native';

import API from '../api/ApiRequest';
import DB from '../database/DB';
import Helper from '../utils/Helper';
import Choose from './fragments/FragChooseCategory';
import NoConnection from './fragments/FragNoConnection';
import Session from '../utils/Session';
import Constant from '../utils/Constant';
/**
 * The first screen in app.
 * It will fetch posts/categories if it is first run
 * i) Fetches categories
 * ii) Allowes user to choose categories
 * iii) Fetches post for initial setup
 */
export default class Splash extends Component {

  static navigationOptions = {
    header: null
  };

  getView() {
    switch (this.state.level) {
      case -1: // we need connection to setup for the first time.
      return <NoConnection />;
      case 0: // simple loading
      return <View style={{ backgroundColor: '#fff', flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ textAlign: 'center', fontSize: 32, color: '#65a', fontWeight: 'bold' }}>safalstories</Text>
        <Text style={{ textAlign: 'center', fontSize: 14, color: '#455' }}>loading.</Text>
      </View>; 
      case 1: // categories fetched
      return <Choose navigation={this.props.navigation} categories={this.state.fetched.category} />
    }
  }

  async fetchCategory(page) {
    // may be the categories are already selected we may need to jump
    const selected = DB.getSelectedCategory();
    if (selected.length !== 0) {
      return this.props.navigation.navigate(Constant.NAV_MAIN);
    }
    const categories = await API.getCategories(page);
    this.state.fetched.category = this.state.fetched.category.concat(categories);
    if (categories.length == 50) {
      this.fetchCategory(++page);
    } else {
      this.setState({ level: 1 });
      this.saveCategory();
    }
  }

  saveCategory() {
    if (this.state.fetched.category.length == 0) {
      return this.startSession();
    }
    const category = this.state.fetched.category.pop();
    DB.storeCategory(category.name, category.id, category.slug, false);
    const context = this;
    requestAnimationFrame(() => context.saveCategory());
  }

  constructor(props) {
    super(props);
    this.state = {
      level: 0,
      isFirstRun: -1,
      fetched: {
        category: [],
        posts: []
      }
    };
    this.runFirstTest();
  }
  
  checkConnection() {
    const ctx = this;
    NetInfo.isConnected.fetch().then((value) => {
      ctx.make(value);
    });
    NetInfo.isConnected.addEventListener('change', (connected) => {
     ctx.make(connected);
    });
  }

  make(connected) {
    while (this.state.isFirstRun == -1);
    if (this.state.isFirstRun !== Constant.KEY_NOT_FIRST_RUN) {
      if (connected) {
        this.fetchCategory(1);
      } else {
        this.setState({
          level: -1 // flag to set we are not connected to the internet
        });
      }
    }
  }

  async runFirstTest() {
    // are we running for the first time?
    const isFirstRun = await Helper.isFirstRun();
    this.setState({
      isFirstRun: isFirstRun
    });
    Session.put('should_open', isFirstRun !== Constant.KEY_NOT_FIRST_RUN);
    if (isFirstRun === Constant.KEY_NOT_FIRST_RUN) {
      this.startSession();
      this.props.navigation.navigate(Constant.NAV_MAIN);
    } else {
      this.checkConnection();
    }
  }

  startSession() {
    const categories = DB.getCategories();
    Session.put(Constant.KEY_CATEGORY, categories);
  }

  render() {
    return this.getView();
  }
}