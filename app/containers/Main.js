import React, { Component } from 'react';
import { StyleSheet, View, Button, Text, Modal, TouchableOpacity, ListView, NetInfo, Linking } from 'react-native';
import SideMenu from 'react-native-drawer';
import { Icon } from 'react-native-elements';

import MenuComponent from '../components/MenuComponent';
import FragFeed from './fragments/FragFeed';
import Helper from '../utils/Helper';
import Store from '../utils/Store';
import Session from '../utils/Session';
import MainHeader from '../components/MainHeader';
import DB from '../database/DB';
import API from '../api/ApiRequest';
import Constant from '../utils/Constant';
import ChooseCategory from '../containers/fragments/FragChooseCategory';

export default class Main extends Component {
  static navigationOptions = {
    title: 'Home',
    header: null
  };

  static connectionStatus = {
    NOT_CONNECTED: 0,
    CONNECTED: 1,
    CONNECTING: 2
  };

  constructor(props) {
    super(props);
    const shouldOpen = Session.get('should_open', true).value;
    this.state = {
      item: {
        title: 'Feed',
        screen: FragFeed
      },
      index: 0,
      netStatus: Main.connectionStatus.CONNECTING,
      isMenuOpen: shouldOpen,
      chooseCategory: false,
      categories: []
    };
    Session.put(Constant.KEY_FEED_TYPE, 'default');
    NetInfo.isConnected.addEventListener('change', (isConnected => {
      const status = isConnected ? Main.connectionStatus.CONNECTED : Main.connectionStatus.NOT_CONNECTED;
      this.setState({ netStatus: status });
    }).bind(this));
  }

  componentDidMount() {
    let categories = DB.getCategories().map(cat => {
      return {
        id: cat.id,
        chosen: cat.chosen,
        name: cat.name
      }
    });
    this.setState({ categories });
  }

  onChangeCategoryPressed() {
    this.setState({ chooseCategory: true });
  }

  async onAddPressed() {
    await Linking.openURL('mailto:safalstories@gmail.com?subject=Title+Here&body=Your+story.+You+can+add+images+too');
    // this.props.navigation.navigate('AddPost');
  }

  onMenuPressed() {
    this._drawer.open();
  }

  onItemPressed(index, item) {
    setTimeout(() => Session.put(Constant.KEY_FEED_TYPE, item.arg || null), 1);
    this.setState({ item, index });
    this._drawer.close();
  }

  render() {
    if (this.state.chooseCategory) {
      return <ChooseCategory navigation={this.props.navigation} categories={this.state.categories} existing={true} />
    }
    let Current = this.state.item;
    const CurrentScreen = Current.screen;
    const MenuComp = <MenuComponent currentIndex={this.state.index} _onItemPressed={this.onItemPressed.bind(this)} />;
    return (
      <SideMenu negotiatePan={true} useInteractionManager={true} ref={(ref) => this._drawer = ref }
        tapToClose={true} style={drawerStyles} openDrawerOffset={0.2} 
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        tweenHandler={(ratio) => ({
          main: { opacity:(2-ratio)/2 }
        })}
        type='overlay' open={this.state.isMenuOpen} content={MenuComp}>
        <MainHeader isAddPage={this.state.index === 6}  _onAddPressed={this.onChangeCategoryPressed.bind(this)}  _onMenuPressed={this.onMenuPressed.bind(this)} title={Current.title} />
        <CurrentScreen  arg={this.state.arg} nav={this.props.navigation} />
        <TouchableOpacity activeOpacity={.5} onPress={this.onAddPressed.bind(this)} style={{ elevation: 4, backgroundColor: 'green', borderRadius: 52, height: 52, width: 52, position: 'absolute', right: 16, bottom: 16 }}>
          <Icon name='add' color='#fff' size={28} style={{ padding: 12 }} />
        </TouchableOpacity>
      </SideMenu>
    );
  }

}

const drawerStyles = StyleSheet.create({
  drawer: {
    flex: 1,
    shadowColor: '#000000'
  },
  drawerOverlay: {
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowRadius: 10,
  },
  mainOverlay: {
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowRadius: 10,
  },
  main: {
    flex: 1,
    shadowColor: '#000000'
  }
});