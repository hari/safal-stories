import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView } from 'react-native';
import Carousel from 'react-native-looped-carousel';

import NoConnection from './FragNoConnection';
import API from '../../api/ApiRequest';

const { width, height } = Dimensions.get('window');

export default class FragGOI extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: 0,
      size: { width, height },
      images: []
    };
    this.fetchImages();
  }

  _onLayoutDidChange = (e) => {
    const layout = e.nativeEvent.layout;
    this.setState({ size: { width: layout.width, height: layout.height } });
  }

  async fetchImages() {
    const ids = await API.getGOI();
    if (ids.length === 0) {
      return this.setState({  status : 2 });
    }
    this.fetchAndShow(ids);
  }

  async fetchAndShow(ids) {
    const medias = await API.getMedias(ids);
    this.setState({
      images: medias.map(media => {
        return {
          id: media.id,
          title: media.title.rendered,
          src: media.source_url,
          height: media.media_details.height,
          width: media.media_details.width,
        }
      }),
      status: 3
    });
  }
  
  render() {
    const status = this.state.status;
    switch (status) {
      case 0:
      return this.getLoading();
      case 1:
      return this.getNotConnected();
      case 2:
      return this.failed();
      default:
      return this.getImages();
    }
  }

  getImages() {
    const images = this.state.images.map(image => {
      const width = this.state.size.width;
      const height = parseInt(width * image.height/image.width);
      return (
        <ScrollView key={image.id} style={this.state.size}>
          <Image resizeMethod='resize' resizeMode='cover' source={{ uri: image.src, width: width, height: height }} />
          <Text style={{ padding: 8, textAlign: 'center', fontSize: 14, color: '#896979', fontWeight: 'bold' }}>{image.title}</Text>
        </ScrollView>
      );
    })
    return (
      <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
        <Carousel
          style={this.state.size}
          autoplay={false}
          pageInfo
        >
        {images}
        </Carousel>
      </View>
    );
  }

  getNotConnected() {
    return <NoConnection />;
  }

  getLoading() {
    return (
      <View style={{ backgroundColor: '#fff', paddingTop: 8, paddingBottom: 8, paddingLeft: 16, paddingRight: 8 }}>
        <Text>Loading..</Text>
      </View>
    );
  }

  failed() {
    return (
      <View style={{ backgroundColor: '#fff', paddingTop: 8, paddingBottom: 8, paddingLeft: 16, paddingRight: 8 }}>
        <Text>We couldn't load content :(</Text>
      </View>
    );
  }
}