import React, { Component } from 'react';
import { Text, FlatList, Alert, NetInfo } from 'react-native';

import API from '../../api/ApiRequest';
import Post from '../../components/Feed/Post';
import DB from '../../database/DB';
import Session from '../../utils/Session';
import Constant from '../../utils/Constant';

export default class FragFeed extends Component {

  constructor(props) {
    super(props);
    const ctx = this;
    this.state = {
      posts: [],
      refreshing: false,
      callbackIndex: 0,
      type: 'default',
      start: 0,
      limit: 6,
      isConnected: true
    };
    NetInfo.isConnected.fetch().then((isConnected => {
      this.setState({ isConnected });
    }).bind(this))
    NetInfo.addEventListener('change', (ic => {
      this.setState({
        isConnected: ic !== 'none'
      })
    }).bind(this));
  }

  componentWillMount() {
    const index = Session.subscribe(this.onValueChange.bind(this)) - 1;
    this.setState({
      callbackIndex: index
    });
    const ctx = this;
    setTimeout(() => ctx.loadPost(), 2);
  }

  componentWillUnmount() {
    Session.unsubscribe(this.state.callbackIndex);
  }

  // handles page change
  onValueChange(item) {
    if (item.key != Constant.KEY_FEED_TYPE || item.value == null || this.state.current == item.value) {
      return;
    }
    this.setState({
      type: item.value,
      posts: [],
      start: 0,
      fetched: false
    }, this.loadPost);
  }

  loadPost() {
    const posts = this.state.posts;
    const excludeCategories = this.mapToCategory(this.state.type);
    let _posts = DB.getPosts();
    if (excludeCategories[0] === -1) {
      _posts = _posts.filter(post => post.liked === true);
    } else {
      _posts = _posts.filter(post => {
                const parsed = post.categories.split(',').map(cid => parseInt(cid));
                for(i in parsed) {
                  if (excludeCategories.indexOf(parsed[i]) !== -1) {
                    return true;
                  }
                }
                return false;
              });
    }
    const end = this.state.start + this.state.limit;
    if (end < _posts.length) {
      _posts = _posts.slice(this.state.start, end);
    } else {
      _posts = _posts.slice(this.state.start);
    }
    for(p in _posts) {
      const post = _posts[p];
      posts.push(post);
    }
    this.setState({ posts: posts, fetched: true });
  }
  
  mapToCategory(type) {
    const map = {
      loved: [-1], // this is our custom category
      story: [15], // these values are from wordpress
      tips: [18]
    };
    if (map.hasOwnProperty(type)) {
      return map[type];
    }
    const categories = Session.get(Constant.KEY_CATEGORY, []).value.filter(category => {
      let include = true;
      for(i in map) {
        if (map[i].indexOf(category.id) !== -1) {
          include = false;
          break;
        }
      }
      return include;
    }).map(category => category.id);
    return categories;
  }


  async onRefresh() {
    if (this.state.isConnected === true) {
      const cats = DB.getCategories().filter(cat => cat.chosen).map(cat => cat.id).join(',');
      const psts = DB.getPosts().map(post => post.id).join(',');
      const params = 'categories='+ cats + '&exclude=' + psts;
      const posts = await API.getPosts(1, params);
      posts.forEach(post => this.savePost(post));
      this.setState({ refreshing: false }, this.loadPost);
    } else {
      this.setState({ refreshing: false });
      Alert.alert('Opps', 'Internet is required to update stories.');
    }
  }
  
  savePost(post) {
    requestAnimationFrame(() => {
      if (!post || !post.title) return;
      DB.addPost(post.id, 
              post.title.rendered,
              post.excerpt.rendered, 
              post.content.rendered,
              post.categories.join(','), 
              post.featured_media + '',
              post.tags.join(','),
              new Date(post.date));
    });
  }

  loadMore() {
    const newStart = this.state.start + this.state.limit;
    this.setState({
      start: newStart
    }, this.loadPost);
  }

  render() {
    if (this.state.posts.length == 0 && !this.state.fetched) {
      return <Text style={{ padding: 16, backgroundColor: '#fff' }}>Loading</Text>;
    }
    if (this.state.posts.length == 0 && this.state.fetched) {
      return <Text style={{ padding: 16, backgroundColor: '#fff' }}>No posts found :(</Text>;
    }
    const { nav } = this.props;
    return (
      <FlatList refreshing={this.state.refreshing} onRefresh={this.onRefresh.bind(this)} 
        onEndReached={this.loadMore.bind(this)}
        onEndReachedThreshold={this.state.limit}
        style={{ paddingTop: 8, paddingBottom: 8 }} data={this.state.posts} keyExtractor={(item, index) => index } 
        renderItem={ post => <Post nav={nav} liked={false} post={post.item} /> } />
    );
  } 
}