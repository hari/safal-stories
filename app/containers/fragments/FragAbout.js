import React, { Component } from 'react';
import { ScrollView, Text, View, StyleSheet, Linking } from 'react-native';
import { Icon } from 'react-native-elements';

export default class FragSubscribe extends Component {

  openUrl(type) {
    let url;
    switch (type) {
      case 'fb':
      url = 'https://www.facebook.com/safalstories/';
      break;
      case 'tw':
      url = 'https://twitter.com/safalstories/';
      break;
      case 'in':
      url = 'https://www.instagram.com/safalstories/';
      break;
      case 'yt':
      url = 'https://www.youtube.com/channel/UCNDcMz9nz4m_zwGsrTm5Ehw';
      break;
    }
    Linking.openURL(url);
  }

  render() {
   return (
      <ScrollView style={{ flex: 1, flexDirection: 'column',  backgroundColor: '#fff', padding: 16 }}>
        <View style={{ padding: 32, justifyContent: 'center', flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
          <Text style={{ fontWeight: 'bold', fontSize: 24, color: '#FF0019' }}>Safal</Text>
          <Text style={{ fontWeight: 'bold', fontSize: 24, color: '#3F51B5' }}>Stories</Text>
        </View>
        <Text style={{ textAlign: 'center' }}>Safal Stories is carefully loaded with inspiring articles and stories to help you build Safal Career.</Text>
        <Text style={{ paddingTop: 8, textAlign: 'center', color: '#999' }}>Find on us on: </Text>
        <View style={{ padding: 8, justifyContent: 'center', flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
          <Icon activeOpacity={.8} onPress={ this.openUrl.bind(this, 'fb') } iconStyle={{ textAlign: 'center' }} type='font-awesome' size={20} color='#fff' style={styles.icon} name='facebook' />
          <Icon activeOpacity={.8} onPress={ this.openUrl.bind(this, 'tw') } iconStyle={{ textAlign: 'center' }} type='font-awesome' size={20} color='#fff' style={styles.icon} name='twitter' />
          <Icon activeOpacity={.8} onPress={ this.openUrl.bind(this, 'in') } iconStyle={{ textAlign: 'center' }} type='font-awesome' size={20} color='#fff' style={styles.icon} name='instagram' />
          <Icon activeOpacity={.8} onPress={ this.openUrl.bind(this, 'yt') } iconStyle={{ textAlign: 'center' }} type='font-awesome' size={20} color='#fff' style={styles.icon} name='youtube' />
        </View>
      </ScrollView>
    );
  } 
}

const styles = StyleSheet.create({
  icon: {
    backgroundColor: '#282828',
    borderWidth: 1,
    borderColor: '#101010',
    padding: 4,
    margin: 4,
    width: 32,
    height: 32
  }
});