import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

export default class FragNoConnection extends Component {

  render() {
    return (
      <View style={{ padding: 16, flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
        <Icon name='wifi' size={50} color='#888' />
        <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 24, marginTop: 16, marginBottom: 16 }}>No Connection</Text>
        <Text style={{ textAlign: 'center', color: '#888' }}>Please, connect to the internet for setting up for the first time</Text>
      </View>
    );
  }
}