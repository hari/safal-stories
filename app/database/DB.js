import Realm from 'realm';
import { AllHtmlEntities } from 'html-entities';

import PostSchema from './schemas/PostSchema';
import CategorySchema from './schemas/CategorySchema';
import Constant from '../utils/Constant';
import Session from '../utils/Session';

const realm = new Realm({
  schema: [PostSchema, CategorySchema],
  schemaVersion: 1
});

export default class DB {
  
  static getPosts(start, end) {
    const posts = realm.objects(Constant.KEY_POST);
    if (start != undefined && end != undefined) {
      if (end < posts.length) {
        return posts.slice(start, end);
      } else {
        return posts.slice(start);
      }
    }
    return posts.slice(0);
  }

  static getSelectedCategory() {
    const cats = DB.getCategories();
    if (cats) {
      return cats.slice(0).filter(cat => cat.chosen == true);
    }
    return [];
  }

  static getCategories() {
    return realm.objects(Constant.KEY_CATEGORY).slice(0);
  }

  static getPostById(id) {
    return DB.getPosts().filter(item => item.id == id);
  }

  static getCategoryById(id) {
    return DB.getCategories().filter(item => item.id == id);
  }

  static postExists(id) {
    return DB.getPostById(id).length != 0;
  }

  static categoryExists(id) {
    return DB.getCategoryById(id).length != 0;
  }

  static addPost(id, title, intro, content, category, media, tags, created_on) {
    if (DB.postExists(id)) {
      return false;
    }
    const ent = new AllHtmlEntities();
    realm.write(() => {
      const post = realm.create(Constant.KEY_POST, {
        id: id,
        intro: ent.decode(intro),
        title: ent.decode(title),
        content: ent.decode(content),
        categories: category,
        featured_img: media,
        tags: tags,
        created_on: created_on,
        liked: false
      }, true);
    });
  }

  static storeCategory(name, id, slug, chosen) {
    if (DB.categoryExists(id)) {
      return false;
    }
    realm.write(() => {
      const cat = realm.create(Constant.KEY_CATEGORY, {
        id: id,
        name: name,
        slug: slug,
        chosen: chosen
      }, true);
    });
  }

  static deleteAll(name) {
    realm.write(() => {
      realm.delete(realm.objects(name));
    });
  }

  static updateCategory(id, chosen) {
    const all = DB.getCategoryById(id);
    if (all.length == 0) {
      return;
    }
    const category = all[0];
    if (category.chosen == chosen) {
      return;
    }
    realm.write(() => {
      category.chosen = chosen;
    });
  }

  static updatePost(id, liked) {
    const all = DB.getPostById(id);
    if (all.length == 0) {
      return;
    }
    const post = all[0];
    if (post.liked == liked) {
      return;
    }
    realm.write(() => {
      post.liked = liked;
    });
  }

  static setImage(post, url, done) {
    requestAnimationFrame(() => {
      realm.write(() => {
        post.featured_img = url;
        done(post);
      });
    });
  }

}