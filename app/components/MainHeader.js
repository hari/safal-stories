import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { Header, Icon } from 'react-native-elements';

export default class MainHeader extends Component {
  
  render() {
    return (
      <View elevation={2} style={styles.header}>
        <TouchableOpacity style={{ flex: .1, paddingTop: 2 }} onPress={this.props._onMenuPressed}>
          <Icon name='menu' color='#fff' />
        </TouchableOpacity>
        <Text style={{ paddingLeft: 24, flex: .7, textAlign: 'left', color: '#fff', fontSize: 18, fontWeight: 'bold' }}>{this.props.title}</Text>
        <TouchableOpacity style={{ flex: .2, paddingTop: 2, alignItems: 'flex-end' }} onPress={this.props._onAddPressed}>
          <Icon name='grid' size={14} type='simple-line-icon' color='#fff' />
        </TouchableOpacity>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  header: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    paddingTop: 16,
    flex: 1,
    flexDirection: 'row',
    maxHeight: 50,
    backgroundColor: '#008A7C',
    borderBottomWidth: 1,
    borderBottomColor: '#007B6B',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
});
