import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Badge } from 'react-native-elements';

export default class RoundedButton extends Component {

  constructor(props) {
    super(props);
    const active = this.props.active || false;
    this.state = { active }
  }

  onPress(category) {
    const wasActive = this.state.active;
    this.setState({
      active: !wasActive
    })
    this.props.onPress(!wasActive, category);
  }

  render() {
    const stls = [styles.btn];
    if (this.state.active) {
      stls.push(styles.btnActive);
    }
    return (
      <Badge textStyle={{ color: '#343434', textAlign: 'center', fontWeight: 'bold' }} style={stls} value={this.props.title} onPress={this.onPress.bind(this)} />
    );
  }
}

const styles = StyleSheet.create({
  btn: {
    borderRadius: 24,
    backgroundColor: 'transparent',
    padding: 8,
    borderWidth: 1,
    marginRight: 4,
    marginBottom: 4,
    minWidth: 50
  },
  btnActive: {
    backgroundColor: 'rgb(170, 250, 180)',
    borderColor: 'rgb(130, 230, 140)'
  }
});