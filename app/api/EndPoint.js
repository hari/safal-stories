const BASE_URL = 'http://safalstories.com/wp-json/wp/v2';

export const EndPoint = {
  POSTS: '/posts',
  CATEGORIES: '/categories',
  MEDIAS: '/media',
  SUBSCRIBE: 'http://safalstories.com/subscribe.php',
  GOI: '/pages/697',
  STORY: 'http://safalstories.com/api/create-post.php'
}

export default function getUrl(endPoint) {
  return BASE_URL + endPoint;
}