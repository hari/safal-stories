import makeUrl, { EndPoint } from './EndPoint';
import Store from '../utils/Store';

export default class ApiRequest {
  
  static async post(url, params) {
    try {
      const response = await fetch(url, {  
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify(params)
      });
      return response;
    } catch (error) {
      // 
    }
    return { }
  }

  static async get(url) {
    try {
      return await fetch(url);
    } catch(error) {
      // console.log(error);
    }
    return { }
  }

  static async getSecure(url) {
    try {
      return await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': '*/*',
          'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
        }
      });
    } catch (error) {
      console.log(error);
    }
    return null;
  }

  static async getCategories(page) {
    const params = '?per_page=50&page='+page;
    const response = await ApiRequest.get(makeUrl(EndPoint.CATEGORIES)+params);
    if (!response) {
      return [];
    }
    return await response.json();
  }

  static async getPosts(page, query) {
    const params = '?per_page=100&page='+page+'&'+query;
    const response = await ApiRequest.get(makeUrl(EndPoint.POSTS)+params);
    if (!response) {
      return [];
    }
    return await response.json();
  }

  static async getMedia(id) {
    const response = await ApiRequest.get(makeUrl(EndPoint.MEDIAS+'/'+id));
    if (!response) {
      return null;
    }
    const media = await response.json();
    return media.media_details.sizes.td_533x261.source_url;
  }

  static async getMedias(ids) {
    const response = await ApiRequest.get(makeUrl(EndPoint.MEDIAS+'?per_page=50&include='+ids.join(',')));
    if (!response) {
      return [];
    }
    return await response.json();
  }

  static async testPost() {
    const response = await ApiRequest.get(makeUrl(EndPoint.POSTS));
    return await response.json();
  }

  static async newSubscriber(name, email) {
    const response = await ApiRequest.post(EndPoint.SUBSCRIBE, {
      name: name,
      email: email
    });
    return await response.json();
  }

  static async getGOI() {
    const response = await ApiRequest.getSecure(makeUrl(EndPoint.GOI));
    if (!response) {
      return [];
    }
    const json = await response.json();
    return json.content.rendered.split('images=&#8221;')[1].split('&#8243;')[0].split(',');
  }

  static async newStory(image, title, story) {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('story', story);
    formData.append('image', image);
    // const response = await 
    fetch(EndPoint.STORY, {
      method: 'POST',
      headers: {
        'Content-Type' : 'multipart/form-data'
      },
      body: formData
    }).then(async response => {
      console.log(response.headers);
      console.log(await response.text());
      console.log(response.body);
    }).catch(error => {
      console.log(error);
    });
  }
}